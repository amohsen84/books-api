# Overview

A caching layer implemented based on AOP (Aspect Oriented programming). The current version supports two caching strategies; in memory and Redis. The library can be extended to include more strategies.

You need to quicky go through the following URLs in order to understand how AOP does work:
- https://github.com/k1r0s/kaop-ts
- https://hackernoon.com/aspect-oriented-programming-in-javascript-es5-typescript-d751dda576d0

## How to use the library?

Follow the instructions below to add a caching layer to your application using this library:

1. Set the cacheService in the CacheAdvices class (in inversify.config.ts) with the cache clients as follow:
--------

CacheAdvices.cacheServices = new CacheService(cacheclients);

---------

Where cacheclients is an array of ICacheClient (The library provides InMemory and Redis clients)

2. Set the ttlConfigsMap in the CacheAdvices class as follow:
--------

CacheAdvices.ttlConfigsMap = new Map([
    ["Class.Method1", 5000]
    ["Class.Method2", 7000]
    ["default", 300]
]);

---------

The key in the ttl configs need to be in the following format `{ClassName}.{MethodName}`.


3. Before the definition of each function that performs complex logic or takes sometime to get data, add the following advices:

--------

    @beforeMethod(CacheAdvices.findInCache(jsonParse))
    @afterMethod(CacheAdvices.storeInCache(ttl))

---------

`jsonParse` is an optional argument (default value is true) that indicates whether to JSON parse the cached value or not.

`ttl` will be replaced with an integer (represents time to live in seconds) or a function that calculates ttl based on the arguments passed to the decorated method. This argument is an optional (The configs set in the previous step will be used if it is omitted).

The first advice `@beforeMethod(CacheAdvices.findInCache())` tries to get the data through the specified cache clients in the order provided, the key passed to the client is obatined in the cache advices class based on the name of the decorated method and arguments passed to it.

If the requested data found in one of the caching layers, the rest of caching layers and main method will be skipped.

In case the data is not found in the caching layer, the method is executed then the result is cached in the specified caching layers through the second advice `@afterMethod(CacheAdvices.storeInCache(ttl))`.


In this project the caching mechanism has been added to BookRepository.getAllBooks. Therefore, all request to /books will make use of the caching layer
