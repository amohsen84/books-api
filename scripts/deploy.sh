#!/usr/bin/env bash

check_exit_status () {
  if [ $1 -ge 1 ]
  then
      echo "Serverless command failed!"
      exit 1;
  else
      return 0;
  fi
}

REGION="eu-west-1"
POSITIONAL=()
while [[ $# -gt 0 ]]; do
    key="$1";
    value="$2";

    case ${key} in
        -s|--stage)
            STAGE=${value};
            shift # past argument
            ;;
        -r|--region)
            REGION=${value};
            shift # past argument
            ;;
        *)    # unknown option
            POSITIONAL+=("$1") # save it in an array for later
            shift # past argument
            ;;
    esac
done
set -- "${POSITIONAL[@]}";

if [[ -z "${STAGE}" ]]
then
    echo "Please provide a stage e.g. yarn deploy --stage your_initials"
    exit 1
fi

if [[ -z "${REGION}" ]]
then
    echo "Please provide a region e.g. yarn deploy --stage your_initials --region eu-west-1"
    exit 1
fi


yarn serverless deploy --stage ${STAGE} --region ${REGION} --force
check_exit_status $?
