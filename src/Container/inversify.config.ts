import { CompositeBufferingLogger, ILogger, Logger, LogLevel } from 'fikrah-logger'
import { Container, interfaces } from 'inversify'
import 'reflect-metadata'
import { DynamoStore } from '@shiftcoders/dynamo-easy'
import InversifyContext = interfaces.Context;
import { DynamoDB } from 'aws-sdk'
import Book from '../Entities/Book'
import { ApplicationFactory } from '../Factories/application'
import BookRepository from '../Repositories/BookRepository'
import TYPES from './inversify.types'
import { CacheAdvices, CacheService } from '../Caching';
import InMemoryCache from '../Caching/Clients/InMemoryCache';

const container = new Container()

//
// Bool Binds
//
interface IBooleanBind {
    id: symbol;
    value: boolean;
}

const booleanBinds: IBooleanBind[] = [
]

booleanBinds.forEach((booleanBind: IBooleanBind) => {
  container.bind<boolean>(booleanBind.id).toConstantValue(booleanBind.value)
})

//
// String Binds
//
interface IStringBind {
    id: symbol;
    value: string;
}

const stringBinds: IStringBind[] = [
  { id: TYPES.LogLevel, value: process.env.LOG_LEVEL || 'info' }
]

stringBinds.forEach((stringBind: IStringBind) => {
  container.bind<string>(stringBind.id).toConstantValue(stringBind.value)
})

//
// Number Binds
//
interface INumberBind {
    id: symbol;
    value: number;
}

const numberBinds: INumberBind[] = [
]

numberBinds.forEach((numberBind: INumberBind) => {
  container.bind<number>(numberBind.id).toConstantValue(numberBind.value)
})

//
// Dynamic Binds
//
interface IDynamicBind {
  id: symbol;
  builder: (context: InversifyContext) => any;
}

const dynamicBinds: IDynamicBind[] = [
  {
    id: TYPES.Logger,
    builder: (ctx: InversifyContext) => new CompositeBufferingLogger(
      new Logger(
        console,
        ctx.container.get<string>(TYPES.LogLevel) as LogLevel
      )
    )
  }
]

dynamicBinds.forEach((dynamicBind: IDynamicBind) => {
  container
    .bind(dynamicBind.id)
    .toDynamicValue(dynamicBind.builder)
    .inSingletonScope()
})

//
// Constructors Binds
//
interface IConstructorBind {
    id: symbol;
    constructor: new (...args: any[]) => any;
}

const constructorBinds: IConstructorBind[] = [
  { id: TYPES.BookRepository, constructor: BookRepository }
]

constructorBinds.forEach((constructorBind: IConstructorBind) => {
  container
    .bind(constructorBind.id)
    .to(constructorBind.constructor)
    .inSingletonScope()
})

//
// DynamoStore Binds
//
interface IDynamoStoreBind {
  id: symbol;
  mdsModelConstructor: new (...args: any[]) => any;
}

const dynamoStoreBinds: IDynamoStoreBind[] = [
  { id: TYPES.DynamoStore.Book, mdsModelConstructor: Book }
]

const dynamoDb = new DynamoDB({
  region: 'eu-west-1',
  maxRetries: 3,
  httpOptions: {
    timeout: 5000,
    connectTimeout: 5000
  }
})

dynamoStoreBinds.forEach((dynamoStoreBind: IDynamoStoreBind) => {
  container
    .bind(dynamoStoreBind.id)
    .toDynamicValue(
      () => new DynamoStore(dynamoStoreBind.mdsModelConstructor, dynamoDb)
    )
    .inSingletonScope()
})

//
// APPLICATION
//
const appBinds: IDynamicBind[] = [
  {
    id: TYPES.Application,
    builder: (ctx: InversifyContext) => ApplicationFactory.buildApplicationInstance(
      ctx.container.get(TYPES.Logger)
    )
  }
]

appBinds.forEach((dynamicBind: IDynamicBind) => {
  container
    .bind(dynamicBind.id)
    .toDynamicValue(dynamicBind.builder)
    .inSingletonScope()
})

CacheAdvices.cacheService = new CacheService([new InMemoryCache(container.get<ILogger>(TYPES.Logger))])

export default container;
