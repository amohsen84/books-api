const TYPES: { [index: string]: any } = {
  //
  // Constants
  //
  LogLevel: Symbol.for('LogLevel'),
  //
  // Repositories
  //
  BookRepository: Symbol.for('<BookRepository>'),
  //
  // DynamoStore
  //
  DynamoStore: {
    Book: Symbol.for('<DynamoStore<Book>>')
  },
  //
  // Others
  //
  Logger: Symbol.for('<ILogger>'),
  Application: Symbol.for('<Application>')
  //
}

export default TYPES
