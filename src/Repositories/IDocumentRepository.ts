import { BatchWriteItemOutput } from 'aws-sdk/clients/dynamodb'

import IDocument from '../Entities/IDocument'

export default interface IDocumentRepository<T extends IDocument> {
    batchWriteItems(
        models: T[],
        method: 'put' | 'delete',
    ): Promise<BatchWriteItemOutput>;

    getByPkAndPartialSk(pk: string, sk: string): Promise<T[]>;
}
