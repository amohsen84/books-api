import { DynamoStore } from '@shiftcoders/dynamo-easy'
import { ILogger } from 'fikrah-logger'
import { inject, injectable } from 'inversify'
import TYPES from '../Container/inversify.types'
import Book from '../Entities/Book'
import EEntitiesPartitionKeyPrefix from '../Enums/EEntitiesPartitionKeyPrefix'
import EEntitiesSortKeyPrefix from '../Enums/EEntitiesSortKeyPrefix'
import { afterMethod, beforeMethod } from 'kaop-ts'
import AbstractDocumentRepository from './AbstractDocumentRepository'
import { CacheAdvices } from '../Caching'

@injectable()
export default class BookRepository extends AbstractDocumentRepository<
    Book
  > {
  constructor (
        @inject(TYPES.DynamoStore.Book) store: DynamoStore<Book>,
        @inject(TYPES.Logger) logger: ILogger
  ) {
    super(store, logger)
  }

  @beforeMethod(CacheAdvices.findInCache(true, Book))
  @afterMethod(CacheAdvices.storeInCache())
  public async getAllBooks (): Promise<Book[]> {
    return this.getByPkAndPartialSk(
        `${EEntitiesPartitionKeyPrefix.Book}_1`,
        EEntitiesSortKeyPrefix.Book
    )
  }

  public async getBook (bookId: string): Promise<Book|undefined> {
    const books = await this.getByPkAndPartialSk(
        `${EEntitiesPartitionKeyPrefix.Book}_1`,
        `${EEntitiesSortKeyPrefix.Book}_${bookId}`
    )
    return (books || [])[0]
  }

  public async deleteBook (bookId: string): Promise<void> {
    await this.batchWriteItems([
      {
        pk: `${EEntitiesPartitionKeyPrefix.Book}_1`,
        sk: `${EEntitiesSortKeyPrefix.Book}_${bookId}`
      }] as Book[],
    'delete'
    )
  }

  public async upsertBook (book: Book): Promise<void> {
    await this.batchWriteItems([book], 'put')
  }
}
