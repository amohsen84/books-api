import { DynamoStore } from '@shiftcoders/dynamo-easy'
import { BatchWriteItemOutput } from 'aws-sdk/clients/dynamodb'
import { ILogger, NullLogger } from 'fikrah-logger'
import { injectable } from 'inversify'
import IDocument from '../Entities/IDocument'
import IDocumentRepository from './IDocumentRepository'

@injectable()
export default abstract class AbstractDocumentRepository<T extends IDocument> implements IDocumentRepository<T> {
    private readonly store: DynamoStore<T>;

    private readonly logger: ILogger;

    constructor (
      dynamoStore: DynamoStore<T>,
      logger: ILogger
    ) {
      this.store = dynamoStore
      this.logger = logger || new NullLogger()
    }

    /**
     * upsert/delete models.
     */
    public async batchWriteItems (
      models: T[],
      method: 'put' | 'delete'
    ): Promise<BatchWriteItemOutput> {
      return this.getStore()
        .batchWrite()[method](models)
        .execFullResponse()
    }

    /**
     * Returns all records by pk and an sk that begin with the value provided.
     */
    public async getByPkAndPartialSk (
      pk: string,
      sk: string
    ): Promise<T[]> {
      const query = this.getStore()
        .query()
        .wherePartitionKey(pk)
        .whereSortKey()
        .beginsWith(sk)

      return query.execFetchAll()
    }

    protected keysToDelete (deletes: T[]): T[] {
      return deletes.map(
        ({ pk, sk }) => (({
          pk,
          sk
        } as unknown) as T)
      )
    }

    protected getStore (): DynamoStore<T> {
      return this.store
    }

    protected getLogger (): ILogger {
      return this.logger
    }
}
