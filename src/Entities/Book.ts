import { Model, Property } from '@shiftcoders/dynamo-easy'
import EEntitiesPartitionKeyPrefix from '../Enums/EEntitiesPartitionKeyPrefix'
import EEntitiesSortKeyPrefix from '../Enums/EEntitiesSortKeyPrefix'
import { IsInt, IsUUID, Min, IsString, MinLength } from 'class-validator'
import AbstractDocument from './AbstractDocument'

@Model({ tableName: process.env.DYNAMODB_TABLE_NAME })
export default class Book extends AbstractDocument {
  public static buildPartitionKey (): string {
    return `${EEntitiesPartitionKeyPrefix.Book}_1`
  }

  public static buildSortKey (bookId: string): string {
    return `${EEntitiesSortKeyPrefix.Book}_${bookId}`
  }

    @Property({ name: 'id' })
    @IsUUID()
    public uuid: string;

    @Property({ name: 'name' })
    @IsString()
    @MinLength(1)
    name: string;

    @Property({ name: 'rDate' })
    @IsInt()
    @Min(1)
    releaseDate: number;

    @Property({ name: 'auth' })
    @IsString()
    @MinLength(1)
    authorName: string;

    constructor ({ uuid, name, releaseDate, authorName }: { [key:string]: any}) {
      super(
        Book.buildPartitionKey(),
        Book.buildSortKey(uuid)
      )

      this.uuid = uuid
      this.name = name
      this.releaseDate = Number(releaseDate)
      this.authorName = authorName
    }
}
