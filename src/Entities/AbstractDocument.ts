import { Model, PartitionKey, SortKey } from '@shiftcoders/dynamo-easy'

import IDocument from './IDocument'

@Model()
export default abstract class AbstractDocument implements IDocument {
    @PartitionKey()
    public readonly pk: string;

    @SortKey()
    public readonly sk: string;

    protected constructor (
      pk: string,
      sk: string
    ) {
      this.pk = pk
      this.sk = sk
    }
}
