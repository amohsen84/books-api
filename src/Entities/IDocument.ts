export default interface IDocument {
    pk: string;

    sk: string;
}
