import express, { NextFunction, Request, Response } from 'express'
import { ILogger } from 'fikrah-logger'
import { HttpError, InternalServerError, NotFound } from 'http-errors'
import * as swaggerUi from 'swagger-ui-express'

import * as document from '../swagger.json'
import { authenticateUser } from '../Middleware/authenticate.user'
import { logHTTP } from '../Middleware/logger.middleware'
import * as router from '../Routes'

/**
 *  Creates the main API application
 *  Here middleware are assigned to parse POST data and log request/response info
 *  Also handlers are assigned for logging and returning errors
 * @class  Application
 */
export class ApplicationFactory {
  /**
     * Create a HTTP Application instance
     */
  public static buildApplicationInstance (logger: ILogger): express.Application {
    // Create application instance
    const instance = express()

    // Parse JSON or URL encoded body data
    instance.use(express.urlencoded({ extended: true }))

    instance.use(express.json())

    // Use Swagger
    const showExplorer = false
    const options = {}
    const customCss = ''
    instance.use('/api-docs', swaggerUi.serve, swaggerUi.setup(document, showExplorer, options, customCss, '/favicon.png', null, 'API document'))

    // Authenticate User
    instance.use(authenticateUser)

    // Log request/response middleware
    instance.use(logHTTP(logger))

    // Load from router modules
    router.route(instance)

    // 404 Not Found
    // If no previous route handler has matched the request, this one is called
    instance.use((_req, res) => {
      if (!res.headersSent) {
        throw new NotFound()
      }
    })

    // Error handler
    // This catches any error thrown in the aplication
    // If the error is an HttpError, it is used in the response
    // For all other errors, the error is logged and an Internal Server Error is returned
    instance.use((err: HttpError | Error, _req: Request, res: Response, _next: NextFunction) => {
      if (err instanceof HttpError) {
        // Respond with thrown HTTP Errors
        res.status(err.statusCode)
        res.jsonp({ error: { message: err.message } })
      } else {
        // Log other Errors and respond with Internal Server Error
        logger.error(err)
        const ise = new InternalServerError()
        res.status(ise.statusCode)
        res.jsonp({ message: ise.message })
      }
    })

    return instance
  }
}
