import container from '../Container/inversify.config'
import TYPES from '../Container/inversify.types'
import BookRepository from '../Repositories/BookRepository'
import { Request, Response } from 'express'
import { ILogger } from 'fikrah-logger'
import transformBook from '../Utils/transformBook'

export default async function getBooks (req: Request, res: Response): Promise<void> {
  const bookRepo = container.get<BookRepository>(TYPES.BookRepository)
  const logger = container.get<ILogger>(TYPES.Logger)

  try {
    const books = await bookRepo.getAllBooks()

    res.json({ data: books.map(book => transformBook(book)) })
  } catch (error) {
    logger.error('Failed to get all books.', { error })

    res.status(500).json({ message: 'Failed to get all books.' })
  }
}
