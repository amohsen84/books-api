import container from '../Container/inversify.config'
import TYPES from '../Container/inversify.types'
import BookRepository from '../Repositories/BookRepository'
import { Request, Response } from 'express'
import { ILogger } from 'fikrah-logger'

export default async function deleteBook (req: Request, res: Response): Promise<void> {
  const bookRepo = container.get<BookRepository>(TYPES.BookRepository)
  const logger = container.get<ILogger>(TYPES.Logger)

  try {
    const book = await bookRepo.getBook(req.params.bookId)

    if (book === undefined) {
      res.status(404).json({ response: `The requested book '${req.params.bookId}' does not exist in the system.` })
      return
    }

    await bookRepo.deleteBook(req.params.bookId)

    res.status(201).json({ response: 'Successfully deleted.' })
  } catch (error) {
    logger.error('Failed to create a book.', { error })

    res.status(500).json({ message: 'Failed to delete the book record.' })
  }
}
