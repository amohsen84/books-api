
import { validateSync } from 'class-validator'
import container from '../Container/inversify.config'
import TYPES from '../Container/inversify.types'
import BookRepository from '../Repositories/BookRepository'
import Book from '../Entities/Book'
import { Request, Response } from 'express'
import { ILogger } from 'fikrah-logger'

export default async function createBook (req: Request, res: Response): Promise<void> {
  const bookRepo = container.get<BookRepository>(TYPES.BookRepository)
  const logger = container.get<ILogger>(TYPES.Logger)

  try {
    const book = new Book(req.body)

    const validationErrors = validateSync(book)
    if (validationErrors.length) {
      res.status(400).json({ message: 'Invalid payload', validationErrors })
      return
    }

    await bookRepo.upsertBook(book)

    res.status(201).json({ response: 'Successfully created.' })
  } catch (error) {
    logger.error('Failed to create a book.', { error: error.message })

    res.status(500).json({ message: 'Failed to create the book record.' })
  }
}
