import Book from 'src/Entities/Book'

/**
 * This method returns the Book opbject without pk and sk fields
 *
 * @param book
 * @returns Partial<Book>
 */
export default function transformBook (book: Book): Partial<Book> {
  const { pk, sk, ...otherfields } = book

  return otherfields
}
