import { Router } from 'express'
import createBook from '../Handlers/createBook'
import updateBook from '../Handlers/updateBook'
import deleteBook from '../Handlers/deleteBook'
import getBook from '../Handlers/getBook'
import getBooks from '../Handlers/getBooks'

const router = Router() as Router

/** creat a new record */
router.post('/book', createBook)

/**  update a book */
router.post('/book/:bookId/update', updateBook)

/**  delete a book */
router.post('/book/:bookId/delete', deleteBook)

/** Get book details */
router.get('/book/:bookId', getBook)

/** Get all books */
router.get('/books', getBooks)

export default router
