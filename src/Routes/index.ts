import * as express from 'express'

import home from './home.router'

export const route = (app: express.Application): void => {
  app.use('/api', home)
}
