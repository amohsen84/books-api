// This is the root file for the whole application
// The application is imported and served using submitted arguments
import { Application } from 'express'
import { ILogger } from 'fikrah-logger'
import { argv } from 'yargs'

import container from './Container/inversify.config'
import TYPES from './Container/inversify.types'

// CLI Arguments
const port: number = Number(argv.port) || 3000

// Get instances
const logger = container.get<ILogger>(TYPES.Logger)
const app = container.get<Application>(TYPES.Application)

// Serve application
const httpServer = app.listen(port, () => logger.info(`Serving application on http://localhost:${port}`))
const terminateConnection = () => {
  if (httpServer) {
    httpServer.close()
  }
}

process.on('exit', terminateConnection)
process.on('unhandledRejection', terminateConnection)
process.on('uncaughtException', terminateConnection) // unexpected crash
process.on('SIGINT', terminateConnection) // Ctrl + C (1)
