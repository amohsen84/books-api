import ICacheClient from './Clients/ICacheClient'
import ICacheService from './ICacheService'

export default class CacheService implements ICacheService {
    protected cacheClients: ICacheClient[];

    constructor (cacheClients: ICacheClient[] = []) {
      this.cacheClients = cacheClients
    }

    public async getCachedValue (key: string): Promise<string | undefined> {
      for (const cacheClient of this.cacheClients) {
        const value = await cacheClient.get(key)
        if (value !== undefined) {
          return value
        }
      }

      return undefined
    }

    public async storeInCache (
      key: string,
      value: any,
      ttl: number
    ): Promise<void> {
      return Promise.all(
        this.cacheClients.map(cacheClient =>
          cacheClient.set(
            key,
            typeof value === 'object' ? JSON.stringify(value) : value,
            ttl
          )
        )
      ).then(() => Promise.resolve())
    }
}
