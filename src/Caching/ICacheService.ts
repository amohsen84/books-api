export default interface ICacheService {
    getCachedValue(key: string): Promise<string | undefined>;
    storeInCache(key: string, value: any, ttl: number): Promise<void>;
}
