import { ILogger, NullLogger } from 'fikrah-logger'
import { Metadata } from 'kaop-ts'
import md5 from 'md5'
import sleep from '../Utils/sleep'
import ICacheService from './ICacheService'
import { TtlCalculation } from './types'

export declare type ModelConstructor<T> = new (...args: any[]) => T;

const IN_FLIGHT = 'inFlight'

export default class CacheAdvices {
    // Both Static and dynamic ttl are supported
    public static ttlConfigsMap: Map<string, number>;

    public static cacheService: ICacheService;

    public static logger: ILogger;

    public static findInCache = <T = any>(
        deserializeCachedValue = true,
        model?: ModelConstructor<T>
    ) => async (meta: Metadata<any>) => {
      try {
        const ttlConfigs = CacheAdvices.getTtlConfigs()

        // Nothing to do if the cacheService is not set
        if (!CacheAdvices.cacheService) {
          return
        }

        // The cached key is built based on the function name and passed arguments
        const cachedKey = CacheAdvices.getCacheKey(meta.key, meta.args)

        // Get the cached value if found
        let cachedValue = await CacheAdvices.cacheService.getCachedValue(
          cachedKey
        )

        // We need to wait for some time if it is inflight
        // as the same resource in this case is requested by another request
        if (cachedValue === IN_FLIGHT) {
          await sleep(ttlConfigs.get(IN_FLIGHT))

          cachedValue = await CacheAdvices.cacheService.getCachedValue(
            cachedKey
          )
        }

        // If cached value found, do not execute the decorated function
        // and skip the other advices that should be called before the main method
        if (![undefined, null, IN_FLIGHT].includes(cachedValue)) {
          meta.result = deserializeCachedValue
            ? CacheAdvices.deserialize(cachedValue, model)
            : cachedValue
          meta.prevent()
          meta.skip()
        } else if (cachedValue !== IN_FLIGHT) {
          // The resource is not cached and will be fetched from an external party
          // We will set the cache to "In-flight" to avoid making another request for the same resource
          // Future requests for the same resource will wait for the request to be completed (there is a maximum)
          await CacheAdvices.cacheService.storeInCache(
            cachedKey,
            IN_FLIGHT,
            ttlConfigs.get(IN_FLIGHT)
          )
        }
      } catch (error) {
        CacheAdvices.getLogger().warning(
          'Failed to get data from the cache',
          {
            error
          }
        )
      } finally {
        meta.commit()
      }
    };

    // If ttl is provided, it will override the value provided in the configs
    public static storeInCache = (
      overrideTtl?: number | TtlCalculation
    ) => async (meta: Metadata<any>) => {
      // We will ignore adding to the cache if the main method was not executed
      try {
        if (!meta.prevented) {
          const calculatedTtl = CacheAdvices.getTtl(meta, overrideTtl)

          // meta.key is ClassName.functionName while meta.args
          // represent arguments passed to the method
          const cachedKey = CacheAdvices.getCacheKey(meta.key, meta.args)
          const value = await meta.result
          await CacheAdvices.cacheService.storeInCache(
            cachedKey,
            value,
            calculatedTtl
          )
        }
      } catch (error) {
        CacheAdvices.getLogger().warning(
          'Failed to store data in the cache',
          {
            error
          }
        )
      } finally {
        meta.commit()
      }
    };

    private static getCacheKey (methodName: string, args: any[]) {
      const key = `${methodName}-${JSON.stringify(args)}`
      CacheAdvices.getLogger().debug(`CacheKey: ${key}`)

      return md5(key)
    }

    private static getTtl (
      meta: Metadata<any>,
      overrideTtl: number | TtlCalculation
    ): number {
      // Ignore the configs if the overrideTtl valu
      let calculatedTtl: number | TtlCalculation
      if (overrideTtl !== undefined) {
        calculatedTtl = overrideTtl
      } else {
        const ttlConfigs = CacheAdvices.getTtlConfigs()
        const configKey = `${meta.scope.constructor.name}.${meta.key}`
        calculatedTtl =
                ttlConfigs.get(configKey) || ttlConfigs.get('default')
      }

      return typeof calculatedTtl === 'function'
        ? calculatedTtl(meta)
        : calculatedTtl
    }

    private static getLogger (): ILogger {
      if (undefined === CacheAdvices.logger) {
        CacheAdvices.logger = new NullLogger()
      }

      return CacheAdvices.logger
    }

    private static getTtlConfigs (): Map<string, number> {
      if (undefined === CacheAdvices.ttlConfigsMap) {
        CacheAdvices.ttlConfigsMap = new Map([['default', 36000], ['inflight', 36000]])
      }

      return CacheAdvices.ttlConfigsMap
    }

    private static deserialize<T = any> (
      cachedValue: string,
      model: ModelConstructor<T>
    ): any {
      const deserialized = JSON.parse(cachedValue)
      if (model && Array.isArray(deserialized)) {
        deserialized.forEach(item => {
          item.__proto__ = model.prototype
        })
      } else if (model) {
        deserialized.__proto__ = model.prototype
      }

      return deserialized
    }
}
