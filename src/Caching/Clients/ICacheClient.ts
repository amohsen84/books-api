export default interface ICacheClient {
    get(key: string): Promise<string | undefined>;
    set(
        key: string,
        value: string,
        ttl: number, // In seconds
    ): Promise<boolean>;
}
