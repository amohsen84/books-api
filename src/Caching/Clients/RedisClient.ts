import { ILogger } from 'fikrah-logger'
import RedisClustr from 'redis-clustr'
import ICacheClient from './ICacheClient'

export default class RedisClient implements ICacheClient {
    private readonly logger: ILogger;
    private readonly redisClustr: RedisClustr;

    constructor (logger: ILogger, redisClustr: RedisClustr) {
      this.logger = logger
      this.redisClustr = redisClustr
    }

    public async get (
      cacheKey: string,
      attemptsCount = 1
    ): Promise<string | undefined> {
      try {
        this.logger.debug(`Reading from Redis, key: ${cacheKey}`)

        const data: string = await new Promise(resolve => {
          this.redisClustr.get(
            cacheKey,
            (
              error: Error | undefined,
              result: string | undefined | null
            ) => {
              if (error) {
                this.logger.warning(
                                `Failed to get ${cacheKey} from Redis, error: ${error.message}`
                )

                return resolve(undefined)
              }

              if (!result) {
                this.logger.debug(
                                `${cacheKey}  not found in Redis`
                )

                return resolve(undefined)
              }

              this.logger.debug(
                            `Found data for key ${cacheKey} in Redis`
              )

              resolve(result)
            }
          )
        })

        return data
      } catch (error) {
        if (--attemptsCount === 0) {
          throw error
        }

        return this.get(cacheKey, attemptsCount)
      }
    }

    public async set (
      cacheKey: string,
      value: string,
      ttl: number
    ): Promise<boolean> {
      if (!this.redisClustr) {
        return false
      }

      this.logger.debug(`Adding Data of key '${cacheKey}' to Redis`)

      return new Promise(resolve => {
        this.redisClustr.setex(cacheKey, ttl, value, (error, result) => {
          if (error) {
            this.logger.warning(
                        `Failed to store ${cacheKey} in Redis, error: ${error.message}`
            )

            return resolve(false)
          }
          this.logger.debug(`Found data for key ${cacheKey} in Redis`)
          resolve(true)
        })
      })
    }
}
