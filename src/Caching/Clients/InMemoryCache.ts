import { ILogger } from 'fikrah-logger'
import ICacheClient from './ICacheClient'

interface ICachedEntry {
    value: string;
    expiryDateTime: Date;
}

// This is very simple in memory caching implementation, we are planning to improve it in the future  as follow:
// - Implement a mechanism that auto deletes entries when they are expired.
// - Set a max limit for number of entries.
export default class InMemoryCache implements ICacheClient {
    private cachedEntries: { [key: string]: ICachedEntry } = {};
    private readonly logger: ILogger;

    constructor (logger: ILogger) {
      this.logger = logger
    }

    public async get (cacheKey: string): Promise<string | undefined> {
      this.logger.debug(`Reading from InMemory cache, key: ${cacheKey}`)

      const value = (this.cachedEntries[cacheKey] || {}).value
      if (value) {
        this.logger.debug(
                `Found data for key ${cacheKey} in InMemory cache`
        )
      }

      return value
    }

    public async set (
      cacheKey: string,
      value: string,
      ttl: number
    ): Promise<boolean> {
      this.logger.debug('Adding to InMemory Cache', { cacheKey, value, ttl })

      const currentTime = new Date()
      const expiryDateTime = new Date(currentTime.getTime() + ttl * 1000)

      return !!(this.cachedEntries[cacheKey] = {
        value,
        expiryDateTime
      })
    }
}
