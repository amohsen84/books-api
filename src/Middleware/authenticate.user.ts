import { NextFunction, Request, Response } from 'express'

/**
 * Authenticate User
 * This is a dummy function
 * In reality, this function should  check J-Token passed with the request.
 *
 *
 * @param req Express Request
 * @param res Express Response
 * @param next Express Next Function
 */
export function authenticateUser (req: Request, res: Response, next: NextFunction): void {
  next()
}
