import { NextFunction, Request, Response } from 'express'
import { ILogger } from 'fikrah-logger'

export function logHTTP (logger: ILogger) {
  /**
     * Logger middleware
     * Logs the request method, path and response code
     *
     * @param req Express Request
     * @param res Express Response
     * @param next Express Next Function
     */
  return (req: Request, res: Response, next: NextFunction): void => {
    function requestLogger () {
      res.removeListener('finish', requestLogger)
      res.removeListener('close', requestLogger)
      logger.info(`${res.statusCode} ${req.method} ${req.url}`)
    }
    res.on('finish', requestLogger)
    res.on('close', requestLogger)
    next()
  }
}
