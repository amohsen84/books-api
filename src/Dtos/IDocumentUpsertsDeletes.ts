import IDocument from '../Entities/IDocument'

export default interface IDocumentUpsertsDeletes<T extends IDocument> {
    upserts: T[];
    deletes: IDocument[];
}
