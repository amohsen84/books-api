Overview
=======================
An API service that allows ingesting and listing book data. The projec has been written in typescript and is deployable to AWS.


Installation
---------------
 run the following command to install dependencies:

    > cd {Project-Directory}
    > npm run


Transpilation
---------------------
Run the following commands to transpile it:
    > cd {Project Directory}
    > npm run build


Project Structure
-----------------
    /src
      /Caching       # implementation of caching layers 
      /Container     # Includes implementation of inversify (for dependency injection)
      /Entities      # Includes definitions of all entities used in the project
      /Enums         # Enums used in the Project
      /Factories     # Factories that creates some objects and configure them 
      /Handlers      # Includes endpoints handlers
      /Middleware    # Express Middleware
      /Repositories  # Classes used to retriev data from the database
      /Routes        # Routes definitions (end points)
      /Utils         # Some utilities used by classes and functions in the project
    /tests           # Unit tests
    /features        # Features in BDD style
    /steps           # BDD features implementation


Running the service locally
---------------------------
    > cd {Project Directory}
    > npm start

You need to source the .env file before starting the service in order to get DynamoDB table name and ARN env variables exported. 

Unit Testing
------------
To run the unit tests and generate the coverage document (the document will be generated at {Project Directory}/document), you need to run the following commands:
    > cd {Project Directory}
    > npm run test:coverage

The coverage document will be generated at {Project Directory}/coverage. It includes details about how the unit tests covered the code showing lines covered and not covered by the unit tests.

E2E Tests
----------
The purpose of this test is to test the full life cycle of an arrival log (in our case, ingest then list). This has been implemented using cucumber framework.

To run this test, you need to get the service running then set SERVICE_URL in the .env file, and finally run the following:
    > cd {Project Directory}
    > npm run test:e2e


API Documentation
-----------------
The API documentation is dynamically generated using swagger. You need to update the swagger.json file when you modify/add an api method. The API documentation can be found at:

{Service Url}/api-docs/

It is an interactive documentation that you can use it to test the API.

Demo
----
I deployed this project to AWS and the following base URL can be used to test the 

Deployment
----------
This project can be deployed to AWS lambda function using the following command:
    > npm run deploy -- --stage stageName

I have already deployed the project to AWS project and you can use the following base URL to test the API:
    https://cdlls3nfba.execute-api.eu-west-1.amazonaws.com/ams/api/


Running the service locally
---------------------------
You need to create a DynamoDB table before running the service locally. You may first deploy it to AWS (it will creates a DynamoDB table)

You need to source the .env file before starting the service in order to get DynamoDB table name and ARN env variables exported. 

You can run it locally as follow:
    > cd {Project Directory}
    > npm start


# Caching Overview

The caching has been implemented based on AOP (Aspect Oriented programming). The current version supports two caching strategies; `in memory` and `Redis`. The redis strategy can be used over multiple services while The 'in memory' one is useful only in case of caching per service instance.  

The library can be extended to include more strategies.

You need to quicky go through the following URLs in order to understand how AOP does work:
- https://github.com/k1r0s/kaop-ts
- https://hackernoon.com/aspect-oriented-programming-in-javascript-es5-typescript-d751dda576d0

In this project the caching mechanism has been added to BookRepository.getAllBooks. Therefore, all request to /books will make use of the caching layer

## How to configure the caching in this project?

Follow the instructions below to add a caching layer to your application using this library:

1. Set the cacheService in the CacheAdvices class (in inversify.config.ts) with the cache clients as follow:
--------

CacheAdvices.cacheServices = new CacheService(cacheclients);

---------

Where cacheclients is an array of ICacheClient (The library provides InMemory and Redis clients)

2. Set the ttlConfigsMap in the CacheAdvices class as follow:
--------

CacheAdvices.ttlConfigsMap = new Map([
    ["Class.Method1", 5000]
    ["Class.Method2", 7000]
    ["default", 300]
]);

---------

The key in the ttl configs need to be in the following format `{ClassName}.{MethodName}`.


3. Before the definition of each function that performs complex logic or takes sometime to get data, add the following advices:

--------

    @beforeMethod(CacheAdvices.findInCache(jsonParse))
    @afterMethod(CacheAdvices.storeInCache(ttl))

---------

`jsonParse` is an optional argument (default value is true) that indicates whether to JSON parse the cached value or not.

`ttl` will be replaced with an integer (represents time to live in seconds) or a function that calculates ttl based on the arguments passed to the decorated method. This argument is an optional (The configs set in the previous step will be used if it is omitted).

The first advice `@beforeMethod(CacheAdvices.findInCache())` tries to get the data through the specified cache clients in the order provided, the key passed to the client is obatined in the cache advices class based on the name of the decorated method and arguments passed to it.

If the requested data found in one of the caching layers, the rest of caching layers and main method will be skipped.

In case the data is not found in the caching layer, the method is executed then the result is cached in the specified caching layers through the second advice `@afterMethod(CacheAdvices.storeInCache(ttl))`.


Future Work
-----------
    - Add more unit tests
    - Add more scenarios to E2E tests
    - Implement an authentication mechanism
    - Implement fingers crossed logging
    - Add more details to the API documentation
    - Add more comments to the code
    - Improve in-fligh check in the caching implementation.