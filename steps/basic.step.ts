import { Given, When, Then } from "cucumber";
import axios from "axios";
import dotenv from "dotenv";
import { assert } from "chai";
import "reflect-metadata";

dotenv.config();

const serviceUrl = process.env.SERVICE_URL || "http://localhost:3000";

Given("book data is received", function() {
    this.bookData = {
        uuid: "bbfdc5bc-fe82-11eb-9a03-0242ac130003",
        name: "BookName",
        releaseDate: 15000,
        authorName: "author",
    } 
});

When("I ingest the book", async function() {
    await axios.post(
        `${serviceUrl}/api/book`,
        this.bookData
    );
});

Then("I should be able to list it", async function() {
    const response = await axios.get(
        `${serviceUrl}/api/book/bbfdc5bc-fe82-11eb-9a03-0242ac130003`,
    );

    assert.deepEqual(response.data, {data: this.bookData});
});
