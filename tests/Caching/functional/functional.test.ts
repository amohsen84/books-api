import { NullLogger } from 'fikrah-logger'
import { afterMethod, beforeMethod } from 'kaop-ts'
import CacheAdvices from '../../../src/Caching/CacheAdvices'
import CacheService from '../../../src/Caching/CacheService'
import ICacheClient from '../../../src/Caching/Clients/ICacheClient'

const inMemoryClient = {
  get: jest.fn(),
  set: jest.fn()
} as ICacheClient

const redisClient = {
  get: jest.fn(),
  set: jest.fn()
} as ICacheClient

class DataRepo {
    @beforeMethod(CacheAdvices.findInCache(false))
    @afterMethod(CacheAdvices.storeInCache(() => 1000))
  public async getData () {
    return 'FromMainMethod'
  }
}
CacheAdvices.logger = new NullLogger()

const repo = new DataRepo()
describe('DataRepo', () => {
  afterEach(() => {
    jest.resetAllMocks()
  })

  // We test here the flow of calling advices, getData is just an example of a decorated function
  describe('getData', () => {
    it('does not look at the Redis caching layers and execute the main method if it is found in the first caching layer (InMemory) ', async () => {
      inMemoryClient.get = jest.fn(async () => 'cahedDataFromInMemory')
      setCachingService([inMemoryClient, redisClient])

      const data = await repo.getData()

      expect(inMemoryClient.get).toHaveBeenCalledTimes(1)
      expect(redisClient.get).toHaveBeenCalledTimes(0)
      expect(inMemoryClient.set).toHaveBeenCalledTimes(0)
      expect(redisClient.set).toHaveBeenCalledTimes(0)

      expect(data).toBe('cahedDataFromInMemory')
    })

    it('Checks the caching twice if it is in-flight', async () => {
      inMemoryClient.get = jest.fn(async () => 'inFlight')
      setCachingService([inMemoryClient, redisClient])

      const data = await repo.getData()

      expect(inMemoryClient.get).toHaveBeenCalledTimes(2)
      expect(redisClient.get).toHaveBeenCalledTimes(0)
      expect(inMemoryClient.set).toHaveBeenCalledTimes(1)
      expect(redisClient.set).toHaveBeenCalledTimes(1)

      expect(data).toBe('FromMainMethod')
    })

    it('looks at the Redis caching layers if it is not found in the first caching layer (InMemory)', async () => {
      redisClient.get = jest.fn(async () => 'cahedDataFromRedis')
      setCachingService([inMemoryClient, redisClient])

      const data = await repo.getData()

      expect(inMemoryClient.get).toHaveBeenCalledTimes(1)
      expect(redisClient.get).toHaveBeenCalledTimes(1)
      expect(inMemoryClient.set).toHaveBeenCalledTimes(0)
      expect(redisClient.set).toHaveBeenCalledTimes(0)

      expect(data).toBe('cahedDataFromRedis')
    })

    it('calls the main method if it is not found in any caching layer', async () => {
      setCachingService([inMemoryClient, redisClient])

      const data = await repo.getData()

      expect(inMemoryClient.get).toHaveBeenCalledTimes(1)
      expect(redisClient.get).toHaveBeenCalledTimes(1)
      // It sets the cache twice (the first one is 'inFlight')
      expect(inMemoryClient.set).toHaveBeenCalledTimes(2)
      expect(redisClient.set).toHaveBeenCalledTimes(2)

      expect(data).toBe('FromMainMethod')
    })
  })
})

const setCachingService = (clients: ICacheClient[]) => {
  CacheAdvices.cacheService = new CacheService(clients)
}
