import { ILogger, NullLogger } from 'fikrah-logger'
import RedisClustr from 'redis-clustr'
import RedisClient from '../../../src/Caching/Clients/RedisClient'

const buildMocks = (
  mockRedisClustr: any,
  logger: ILogger = new NullLogger()
): {sut: RedisClient, mockRedisClustr: RedisClustr} => {
  const sut = new RedisClient(logger, mockRedisClustr)

  return {
    sut,
    mockRedisClustr
  }
}

describe('RedisClient', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  beforeEach(() => {
    jest.clearAllMocks()
  })

  describe('get', () => {
    it('checks success response', async () => {
      const { sut, mockRedisClustr } = buildMocks({} as RedisClustr)
      const expectedKey = 'key'

      // mock get function
      mockRedisClustr.get = jest.fn((key, callback) => {
        callback(null, 'value')
      })

      const services = await sut.get(expectedKey)

      expect(services).toBe('value')
    })

    it('checks failed response', async () => {
      const { sut, mockRedisClustr } = buildMocks({} as RedisClustr)
      const expectedKey = '123'

      // mock get function
      mockRedisClustr.get = jest.fn((key, callback) => {
        expect(key).toBe(expectedKey)

        callback(new Error('Error Message'), null)
      })

      const data = await sut.get(expectedKey, 5)

      expect(data).toBe(undefined)
    })

    it('retries with the specified allocated attempts', async () => {
      const { sut, mockRedisClustr } = buildMocks({} as RedisClustr)
      const expectedKey = 'key'

      const maxAttempts = 3
      let noOfAttempts = 0

      // mock get function
      mockRedisClustr.get = jest.fn((key, callback) => {
        expect(key).toBe(expectedKey)

        if (noOfAttempts < maxAttempts - 1) {
          noOfAttempts++

          throw new Error(`Expected error at retry: ${noOfAttempts}`)
        }

        callback(null, 'value')
      })

      const services = await sut.get(expectedKey, maxAttempts)

      expect(services).toBe('value')
    })

    it('throws latest error from client if allocated attempts are exceeded', async () => {
      const { sut, mockRedisClustr } = buildMocks({} as RedisClustr)
      const expectedKey = 'key'

      // mock get function
      mockRedisClustr.get = jest.fn(key => {
        expect(key).toBe(expectedKey)

        throw new Error('The expected error')
      })

      await expect(sut.get(expectedKey, 3)).rejects.toEqual(
        new Error('The expected error')
      )
    })

    it('resolved to undefined if cache is not found in client', async () => {
      const { sut, mockRedisClustr } = buildMocks({} as RedisClustr)
      const expectedKey = 'key'

      // mock get function
      mockRedisClustr.get = jest.fn((key, callback) => {
        expect(key).toBe(expectedKey)

        callback(undefined, null)
      })

      const services = await sut.get(expectedKey)

      expect(services).toBeUndefined()
    })
  })

  describe('set', () => {
    it('checks success response', async () => {
      const { sut, mockRedisClustr } = buildMocks({} as RedisClustr)

      // mock get function
      mockRedisClustr.setex = jest.fn((key, ttl, value, callback) => {
        expect(key).toBe('LinearServices-123')
        expect(value).toBe('value')
        expect(ttl).toBe(1000)
        callback(undefined, null)
      })

      const result = await sut.set('LinearServices-123', 'value', 1000)

      expect(result).toBe(true)
    })

    it('checks success response when cache hostname is undefined', async () => {
      const { sut } = buildMocks(undefined)

      const result = await sut.set('LinearServices-123', 'value')

      expect(result).toBeFalsy()
    })

    it('checks failed response', async () => {
      expect.assertions(4)

      const { sut, mockRedisClustr } = buildMocks({} as RedisClustr)

      // mock get function
      mockRedisClustr.setex = jest.fn((key, ttl, value) => {
        expect(key).toBe('LinearServices-123')
        expect(value).toBe('value')
        expect(ttl).toBe(1000)
        throw new Error('The expected error')
      })

      await expect(sut.set('LinearServices-123', 'value', 1000)).rejects.toEqual(
        new Error('The expected error')
      )
    })
  })
})
