import { ILogger, NullLogger } from 'fikrah-logger'

import InMemoryCache from '../../../src/Caching/Clients/InMemoryCache'

const buildSut = (logger: ILogger = new NullLogger()): InMemoryCache =>
  new InMemoryCache(logger)

describe('InMemoryCache', () => {
  beforeEach(() => {
    jest.resetAllMocks()
  })

  describe('set', () => {
    it('Set key', async () => {
      expect.assertions(1)

      const sut = buildSut()

      const result = await sut.set('key', 'value', 3600)

      expect(result).toBe(true)
    })
  })

  describe('get', () => {
    it('Key not found', async () => {
      expect.assertions(1)

      const sut = buildSut()
      const result = await sut.get('key')

      expect(result).toBe(undefined)
    })

    it('Key found', async () => {
      expect.assertions(1)

      const sut = buildSut()

      await sut.set('key', 'value', 3600)
      const result = await sut.get('key')

      expect(result).toBe('value')
    })
  })
})
