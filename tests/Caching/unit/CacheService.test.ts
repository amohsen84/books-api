import CacheService from '../../../src/Caching/CacheService'
import ICacheClient from '../../../src/Caching/Clients/ICacheClient'

const firstClient = {
  get: jest.fn(),
  set: jest.fn()
} as ICacheClient

const secondClient = {
  get: jest.fn(),
  set: jest.fn()
} as ICacheClient

describe('AbstractCachableService', () => {
  afterEach(() => {
    jest.resetAllMocks()
  })

  describe('getCachedValue', () => {
    it('should not call subsequent clients if the data found in an earlier client', async () => {
      firstClient.get = jest.fn(async () => 'Data')

      const sut = new CacheService([firstClient, secondClient])

      const data = await sut.getCachedValue('key')

      expect(firstClient.get).toHaveBeenCalledTimes(1)
      expect(secondClient.get).toHaveBeenCalledTimes(0)
      expect(firstClient.get).toHaveBeenCalledWith('key')

      expect(data).toBe('Data')
    })

    it('returns undefined if value cannot be found in any client', async () => {
      const sut = new CacheService([firstClient, secondClient])

      const data = await sut.getCachedValue('key')

      expect(firstClient.get).toHaveBeenCalledTimes(1)
      expect(firstClient.get).toHaveBeenCalledWith('key')
      expect(secondClient.get).toHaveBeenCalledTimes(1)
      expect(secondClient.get).toHaveBeenCalledWith('key')

      expect(data).toBeUndefined()
    })
  })

  describe('storeInCache', () => {
    it('calls all cache clients to set data', async () => {
      const sut = new CacheService([firstClient, secondClient])

      await sut.storeInCache('key', 'value', 1000)

      expect(firstClient.set).toHaveBeenCalledWith('key', 'value', 1000)
      expect(secondClient.set).toHaveBeenCalledWith('key', 'value', 1000)
    })

    it('sets data by JSON serializing it', async () => {
      const sut = new CacheService([firstClient, secondClient])

      await sut.storeInCache('key', { 'a-string': 'value' }, 1000)

      const expectedJson = JSON.stringify({ 'a-string': 'value' })
      expect(firstClient.set).toHaveBeenCalledWith('key', expectedJson, 1000)
      expect(secondClient.set).toHaveBeenCalledWith('key', expectedJson, 1000)
    })
  })
})
