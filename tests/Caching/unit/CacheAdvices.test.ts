import { NullLogger, AssertingLogger, LogLevel } from 'fikrah-logger'
import CacheAdvices from '../../../src/Caching/CacheAdvices'
import CacheService from '../../../src/Caching/CacheService'

class TestClass {
    prop: string;
    testFunc () {
      return 'expectedValue'
    }
}

describe('CacheAdvices', () => {
  beforeEach(() => {
    CacheAdvices.logger = new NullLogger()
  })

  afterEach(() => {
    CacheAdvices.cacheService = undefined
    CacheAdvices.logger = undefined
  })

  describe('findInCache', () => {
    it('does not call the cache service if it is not set', async () => {
      const meta = {
        key: 'testMethod',
        args: [1, 5, { key: 'LIN_1875' }],
        prevent: jest.fn(),
        skip: jest.fn(),
        commit: jest.fn()
      } as any

      await CacheAdvices.findInCache()(meta)

      expect(meta.result).toBeUndefined()
      expect(meta.prevent).toHaveBeenCalledTimes(0)
      expect(meta.skip).toHaveBeenCalledTimes(0)
      expect(meta.commit).toHaveBeenCalledTimes(1)
    })

    it('does not set result if cache is not found', async () => {
      const meta = {
        key: 'testMethod',
        args: [1, 5, { key: 'LIN_1875' }],
        prevent: jest.fn(),
        skip: jest.fn(),
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.getCachedValue = jest.fn(
        async () => undefined
      )
      await CacheAdvices.findInCache()(meta)

      // md5 of 'testMethod-[1,5,{"key":"LIN_1875"}]'
      const key = '99ed909b3f8160ca2e4e5cc5e717a716'

      expect(
        CacheAdvices.cacheService.getCachedValue
      ).toHaveBeenCalledWith(key)

      expect(meta.result).toBeUndefined()
      expect(meta.prevent).toHaveBeenCalledTimes(0)
      expect(meta.skip).toHaveBeenCalledTimes(0)
      expect(meta.commit).toHaveBeenCalledTimes(1)
    })

    it('sets result as the value found from the cache', async () => {
      const meta = {
        key: 'testMethod',
        args: [1, 5],
        prevent: jest.fn(),
        skip: jest.fn(),
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.getCachedValue = jest.fn(
        async () => 'Found'
      )
      await CacheAdvices.findInCache(false)(meta)

      // md5 of 'testMethod-[1,5]'
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(
        CacheAdvices.cacheService.getCachedValue
      ).toHaveBeenCalledWith(key)

      expect(meta.result).toBe('Found')
      expect(meta.prevent).toHaveBeenCalledTimes(1)
      expect(meta.skip).toHaveBeenCalledTimes(1)
      expect(meta.commit).toHaveBeenCalledTimes(1)
    })

    it('saves caught error to metadata', async () => {
      const meta = {
        key: 'testMethod',
        args: [1, 5],
        prevent: jest.fn(),
        skip: jest.fn(),
        commit: jest.fn()
      } as any

      const expectedError = new Error('The expected error')
      const mockLogger = new AssertingLogger()
      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.logger = mockLogger

      CacheAdvices.cacheService.getCachedValue = jest.fn(async () => {
        throw expectedError
      })
      await CacheAdvices.findInCache(false)(meta)

      // md5 of 'testMethod-[1,5]'
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(
        CacheAdvices.cacheService.getCachedValue
      ).toHaveBeenCalledWith(key)

      expect(meta.result).toBeUndefined()
      expect(meta.prevent).toHaveBeenCalledTimes(0)
      expect(meta.skip).toHaveBeenCalledTimes(0)
      expect(meta.commit).toHaveBeenCalledTimes(1)
      mockLogger.assertInLogsInOrder([{
        level: LogLevel.WARNING,
        message: 'Failed to get data from the cache',
        context: {
          error: expectedError
        }
      }])
    })

    it('deserializes cached object', async () => {
      const meta = {
        key: 'testMethod',
        args: [1, 5],
        prevent: jest.fn(),
        skip: jest.fn(),
        commit: jest.fn()
      } as any

      const expectedObj = new TestClass()
      expectedObj.prop = 'value'

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.getCachedValue = jest.fn(async () =>
        JSON.stringify(expectedObj)
      )
      await CacheAdvices.findInCache(true, TestClass)(meta)

      // md5 of 'testMethod-[1,5]'
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(
        CacheAdvices.cacheService.getCachedValue
      ).toHaveBeenCalledWith(key)

      expect(JSON.stringify(meta.result)).toStrictEqual(JSON.stringify(expectedObj))
      expect(meta.result.testFunc()).toBe('expectedValue')
      expect(meta.prevent).toHaveBeenCalledTimes(1)
      expect(meta.skip).toHaveBeenCalledTimes(1)
      expect(meta.commit).toHaveBeenCalledTimes(1)
    })

    it('Deserialize cached array', async () => {
      const meta = {
        key: 'testMethod',
        args: [1, 5],
        prevent: jest.fn(),
        skip: jest.fn(),
        commit: jest.fn()
      } as any

      const expectedObj = [new TestClass()]
      expectedObj[0].prop = 'value'

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.getCachedValue = jest.fn(async () =>
        JSON.stringify(expectedObj)
      )
      await CacheAdvices.findInCache(true, TestClass)(meta)

      // md5 of 'testMethod-[1,5]'
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(
        CacheAdvices.cacheService.getCachedValue
      ).toHaveBeenCalledWith(key)

      expect(JSON.stringify(meta.result)).toStrictEqual(JSON.stringify(expectedObj))
      expect(meta.result[0].testFunc()).toBe('expectedValue')
      expect(meta.prevent).toHaveBeenCalledTimes(1)
      expect(meta.skip).toHaveBeenCalledTimes(1)
      expect(meta.commit).toHaveBeenCalledTimes(1)
    })
  })

  describe('storeInCache', () => {
    it('sets a NullLogger if the logger instance has not been set', async () => {
      const meta = {
        key: 'testMethod',
        args: [1, 5],
        prevented: false,
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.logger = undefined
      CacheAdvices.cacheService.storeInCache = jest.fn(
        async () => undefined
      )
      await CacheAdvices.storeInCache(1000)(meta)

      expect(CacheAdvices.logger).toBeInstanceOf(NullLogger)
    })

    it('does not save cache if the main method is prevented', async () => {
      const meta = {
        key: 'testMethod',
        args: [1, 5],
        prevented: true,
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.storeInCache = jest.fn(
        async () => undefined
      )
      await CacheAdvices.storeInCache(1000)(meta)

      expect(
        CacheAdvices.cacheService.storeInCache
      ).toHaveBeenCalledTimes(0)

      expect(meta.commit).toHaveBeenCalledTimes(1)
      expect(meta.exception).toBeUndefined()
    })

    it('saves the cache value when the main method is not prevented', async () => {
      const meta = {
        scope: {
          getCachedValue: jest.fn(async () => '6'),
          storeInCache: jest.fn(async () => undefined)
        },
        key: 'testMethod',
        args: [1, 5],
        prevented: false,
        result: '6',
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.storeInCache = jest.fn(
        async () => undefined
      )
      await CacheAdvices.storeInCache(1000)(meta)

      // md5 of 'testMethod-[1,5]';
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(CacheAdvices.cacheService.storeInCache).toHaveBeenCalledWith(
        key,
        '6',
        1000
      )

      expect(meta.commit).toHaveBeenCalledTimes(1)
      expect(meta.exception).toBeUndefined()
    })

    it('JSON serializes objects for the md5 key when saving to the cache', async () => {
      const meta = {
        scope: {
          getCachedValue: jest.fn(async () => '6'),
          storeInCache: jest.fn(async () => undefined)
        },
        key: 'testMethod',
        args: [1, { 'a-number': 5 }],
        prevented: false,
        result: '6',
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.storeInCache = jest.fn(
        async () => undefined
      )
      await CacheAdvices.storeInCache(1000)(meta)

      // md5 of 'testMethod-[1,{"a-number":5}]';
      const key = '7fac0d13da3e5d5985508f2025566452'

      expect(CacheAdvices.cacheService.storeInCache).toHaveBeenCalledWith(
        key,
        '6',
        1000
      )

      expect(meta.commit).toHaveBeenCalledTimes(1)
      expect(meta.exception).toBeUndefined()
    })

    it('saves caught error to metadata', async () => {
      const meta = {
        scope: {
          getCachedValue: jest.fn(async () => '6'),
          storeInCache: jest.fn(async () => undefined)
        },
        key: 'testMethod',
        args: [1, 5],
        prevented: false,
        result: '6',
        commit: jest.fn()
      } as any
      const mockLogger = new AssertingLogger()
      const expectedError = new Error('The expected error')

      CacheAdvices.logger = mockLogger
      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.storeInCache = jest.fn(async () => {
        throw expectedError
      })
      await CacheAdvices.storeInCache(1000)(meta)

      // md5 of 'testMethod-[1,5]';
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(CacheAdvices.cacheService.storeInCache).toHaveBeenCalledWith(
        key,
        '6',
        1000
      )
      expect(meta.commit).toHaveBeenCalledTimes(1)
      mockLogger.assertInLogsInOrder([{
        level: LogLevel.WARNING,
        message: 'Failed to store data in the cache',
        context: {
          error: expectedError
        }
      }])
    })

    it('Should use the TTL static configs for the called method when saving the cache value', async () => {
      CacheAdvices.ttlConfigsMap = new Map(
        [
          ['TestClass.testMethod', 5000]
        ]
      )

      const meta = {
        scope: {
          getCachedValue: jest.fn(async () => '6'),
          storeInCache: jest.fn(async () => undefined),
          constructor: {
            name: 'TestClass'
          }
        },
        key: 'testMethod',
        args: [1, 5],
        prevented: false,
        result: '6',
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.storeInCache = jest.fn(
        async () => undefined
      )
      await CacheAdvices.storeInCache()(meta)

      // md5 of 'testMethod-[1,5]';
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(CacheAdvices.cacheService.storeInCache).toHaveBeenCalledWith(
        key,
        '6',
        5000
      )

      expect(meta.commit).toHaveBeenCalledTimes(1)
      expect(meta.exception).toBeUndefined()
    })

    it('Should use the default TTL in the config for the called method when saving the cache value', async () => {
      CacheAdvices.ttlConfigsMap = new Map(
        [
          ['default', 7000]
        ]
      )

      const meta = {
        scope: {
          getCachedValue: jest.fn(async () => '6'),
          storeInCache: jest.fn(async () => undefined),
          constructor: {
            name: 'TestClass'
          }
        },
        key: 'testMethod',
        args: [1, 5],
        prevented: false,
        result: '6',
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.storeInCache = jest.fn(
        async () => undefined
      )
      await CacheAdvices.storeInCache()(meta)

      // md5 of 'testMethod-[1,5]';
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(CacheAdvices.cacheService.storeInCache).toHaveBeenCalledWith(
        key,
        '6',
        7000
      )

      expect(meta.commit).toHaveBeenCalledTimes(1)
      expect(meta.exception).toBeUndefined()
    })

    it('Should use the override ttl rather than the configs', async () => {
      CacheAdvices.ttlConfigsMap = new Map(
        [
          ['TestClass.testMethod', 9000]
        ]
      )

      const meta = {
        scope: {
          getCachedValue: jest.fn(async () => '6'),
          storeInCache: jest.fn(async () => undefined),
          constructor: {
            name: 'TestClass'
          }
        },
        key: 'testMethod',
        args: [1, 5],
        prevented: false,
        result: '6',
        commit: jest.fn()
      } as any

      CacheAdvices.cacheService = new CacheService()
      CacheAdvices.cacheService.storeInCache = jest.fn(
        async () => undefined
      )
      await CacheAdvices.storeInCache(() => 1000)(meta)

      // md5 of 'testMethod-[1,5]';
      const key = '91155a368ff36ad1223a82270bf60d33'

      expect(CacheAdvices.cacheService.storeInCache).toHaveBeenCalledWith(
        key,
        '6',
        1000
      )

      expect(meta.commit).toHaveBeenCalledTimes(1)
      expect(meta.exception).toBeUndefined()
    })
  })
})
