import express from 'express'
import { NullLogger } from 'fikrah-logger'
import container from '../../src/Container/inversify.config'
import TYPES from '../../src/Container/inversify.types'
import getBooks from '../../src/Handlers/getBooks'
import BookRepository from '../../src/Repositories/BookRepository'

const req = {} as express.Request
const res = {} as express.Response
const mockBookRepo = {} as BookRepository

describe('getBooks', () => {
  beforeAll(() => {
    container.snapshot()
    container.rebind(TYPES.BookRepository).toConstantValue(mockBookRepo)
    container.rebind(TYPES.Logger).toConstantValue(new NullLogger())
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  afterAll(() => {
    container.restore()
  })

  it('returns the book data', async () => {
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }

    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.getAllBooks = jest.fn(async () => [{
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author',
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }])

    await getBooks(req, res)

    expect(mockBookRepo.getAllBooks).toHaveBeenCalledTimes(1)
    expect(res.json).toHaveBeenCalledWith({
      data: [{
        uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
        name: 'BookName',
        releaseDate: 15000,
        authorName: 'author'
      }]
    })
  })

  it('returns 500 in case of error occurrance', async () => {
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }

    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.getAllBooks = jest.fn(async () => { throw new Error('Failed to get data') })

    await getBooks(req, res)

    expect(mockBookRepo.getAllBooks).toHaveBeenCalledTimes(1)
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ message: 'Failed to get all books.' })
  })
})
