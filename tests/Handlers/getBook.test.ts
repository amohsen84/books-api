import express from 'express'
import { NullLogger } from 'fikrah-logger'
import container from '../../src/Container/inversify.config'
import TYPES from '../../src/Container/inversify.types'
import Book from '../../src/Entities/Book'
import getBook from '../../src/Handlers/getBook'
import BookRepository from '../../src/Repositories/BookRepository'

const req = {} as express.Request
const res = {} as express.Response
const mockBookRepo = {} as BookRepository

describe('getBook', () => {
  beforeAll(() => {
    container.snapshot()
    container.rebind(TYPES.BookRepository).toConstantValue(mockBookRepo)
    container.rebind(TYPES.Logger).toConstantValue(new NullLogger())
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  afterAll(() => {
    container.restore()
  })

  it('returns the book data', async () => {
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }

    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.getBook = jest.fn(async () => ({
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author',
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }) as Book)

    await getBook(req, res)

    expect(mockBookRepo.getBook).toHaveBeenCalledWith('bbfdc5bc-fe82-11eb-9a03-0242ac130003')
    expect(res.json).toHaveBeenCalledWith({
      data: {
        uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
        name: 'BookName',
        releaseDate: 15000,
        authorName: 'author'
      }
    })
  })

  it('returns 500 in case of error occurrance', async () => {
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }

    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.getBook = jest.fn(async () => { throw new Error('Failed to get data') })

    await getBook(req, res)

    expect(mockBookRepo.getBook).toHaveBeenCalledWith('bbfdc5bc-fe82-11eb-9a03-0242ac130003')
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ message: 'Failed to find the book data.' })
  })

  it('returns 404 if the book does not exist', async () => {
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }
    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.getBook = jest.fn()

    await getBook(req, res)

    expect(mockBookRepo.getBook).toHaveBeenCalledWith('bbfdc5bc-fe82-11eb-9a03-0242ac130003')
    expect(res.status).toHaveBeenCalledWith(404)
    expect(res.json).toHaveBeenCalledWith({ response: 'The requested book \'bbfdc5bc-fe82-11eb-9a03-0242ac130003\' does not exist in the system.' })
  })
})
