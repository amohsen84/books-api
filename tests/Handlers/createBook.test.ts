import express from 'express'
import { NullLogger } from 'fikrah-logger'
import container from '../../src/Container/inversify.config'
import TYPES from '../../src/Container/inversify.types'
import createBook from '../../src/Handlers/createBook'
import BookRepository from '../../src/Repositories/BookRepository'

const req = {} as express.Request
const res = {} as express.Response
const mockBookRepo = {} as BookRepository

describe('createBook', () => {
  beforeAll(() => {
    container.snapshot()
    container.rebind(TYPES.BookRepository).toConstantValue(mockBookRepo)
    container.rebind(TYPES.Logger).toConstantValue(new NullLogger())
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  afterAll(() => {
    container.restore()
  })

  it('creates the book record and returns 201 if the paylod is valid', async () => {
    req.body = {
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author'
    }

    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.upsertBook = jest.fn()

    await createBook(req, res)

    expect(mockBookRepo.upsertBook).toHaveBeenCalledWith({
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author'
    })

    expect(res.status).toHaveBeenCalledWith(201)
    expect(res.json).toHaveBeenCalledWith({ response: 'Successfully created.' })
  })

  it('returns 400 in case of invalid payload', async () => {
    req.body = {
      uuid: '0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author'
    }

    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.upsertBook = jest.fn()

    await createBook(req, res)

    expect(mockBookRepo.upsertBook).not.toHaveBeenCalled()
    expect(res.status).toHaveBeenCalledWith(400)
    expect(res.json).toHaveBeenCalledWith({
      message: 'Invalid payload',
      validationErrors: [{
        children: [],
        constraints: {
          isUuid: 'uuid must be a UUID'
        },
        property: 'uuid',
        target: {
          authorName: 'author',
          name: 'BookName',
          pk: 'book_1',
          sk: 'Book_0242ac130003',
          releaseDate: 15000,
          uuid: '0242ac130003'
        },
        value: '0242ac130003'
      }]
    })
  })

  it('returns 500 in case of error occurrance', async () => {
    req.body = {
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author'
    }

    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.upsertBook = jest.fn(async () => { throw Error('Upsert error') })

    await createBook(req, res)

    expect(mockBookRepo.upsertBook).toHaveBeenCalledWith({
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author'
    })
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ message: 'Failed to create the book record.' })
  })
})
