import express from 'express'
import { NullLogger } from 'fikrah-logger'
import container from '../../src/Container/inversify.config'
import TYPES from '../../src/Container/inversify.types'
import Book from '../../src/Entities/Book'
import updateBook from '../../src/Handlers/updateBook'
import BookRepository from '../../src/Repositories/BookRepository'

const req = {} as express.Request
const res = {} as express.Response
const mockBookRepo = {} as BookRepository

describe('updateBook', () => {
  beforeAll(() => {
    container.snapshot()
    container.rebind(TYPES.BookRepository).toConstantValue(mockBookRepo)
    container.rebind(TYPES.Logger).toConstantValue(new NullLogger())
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  afterAll(() => {
    container.restore()
  })

  it('updates the book record and returns 201 if the paylod is valid', async () => {
    req.body = {
      name: 'BookName-1'
    }
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }

    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.getBook = jest.fn(async () => ({
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author',
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }) as Book)
    mockBookRepo.upsertBook = jest.fn()

    await updateBook(req, res)

    expect(mockBookRepo.getBook).toHaveBeenCalledWith('bbfdc5bc-fe82-11eb-9a03-0242ac130003')
    expect(mockBookRepo.upsertBook).toHaveBeenCalledWith({
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName-1',
      releaseDate: 15000,
      authorName: 'author'
    })
    expect(res.status).toHaveBeenCalledWith(201)
    expect(res.json).toHaveBeenCalledWith({ response: 'Successfully updated.' })
  })

  it('returns 400 in case of invalid payload', async () => {
    req.body = {
      releaseDate: 'ddddd'
    }
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }
    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.upsertBook = jest.fn()
    mockBookRepo.getBook = jest.fn(async () => ({
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author',
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }) as Book)

    await updateBook(req, res)

    expect(mockBookRepo.getBook).toHaveBeenCalledWith('bbfdc5bc-fe82-11eb-9a03-0242ac130003')
    expect(mockBookRepo.upsertBook).not.toHaveBeenCalled()
    expect(res.status).toHaveBeenCalledWith(400)
    expect(res.json).toHaveBeenCalledWith({
      message: 'Invalid payload',
      validationErrors: [{
        children: [],
        constraints: {
          isInt: 'releaseDate must be an integer number',
          min: 'releaseDate must not be less than 1'
        },
        property: 'releaseDate',
        target: {
          authorName: 'author',
          name: 'BookName',
          pk: 'book_1',
          sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003',
          uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
          releaseDate: NaN
        },
        value: NaN
      }]
    })
  })

  it('returns 500 in case of error occurrance', async () => {
    req.body = {
      name: 'BookName-1'
    }
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }
    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.getBook = jest.fn(async () => ({
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName',
      releaseDate: 15000,
      authorName: 'author',
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }) as Book)
    mockBookRepo.upsertBook = jest.fn(async () => { throw Error('Upsert error') })

    await updateBook(req, res)

    expect(mockBookRepo.getBook).toHaveBeenCalledWith('bbfdc5bc-fe82-11eb-9a03-0242ac130003')
    expect(mockBookRepo.upsertBook).toHaveBeenCalledWith({
      pk: 'book_1',
      sk: 'Book_bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      uuid: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003',
      name: 'BookName-1',
      releaseDate: 15000,
      authorName: 'author'
    })
    expect(res.status).toHaveBeenCalledWith(500)
    expect(res.json).toHaveBeenCalledWith({ message: 'Failed to update the book.' })
  })

  it('returns 404 if the book does not exist', async () => {
    req.body = {
      name: 'BookName-1'
    }
    req.params = {
      bookId: 'bbfdc5bc-fe82-11eb-9a03-0242ac130003'
    }
    res.json = jest.fn()
    res.status = jest.fn(() => res)

    mockBookRepo.getBook = jest.fn()
    mockBookRepo.upsertBook = jest.fn()

    await updateBook(req, res)

    expect(mockBookRepo.getBook).toHaveBeenCalledWith('bbfdc5bc-fe82-11eb-9a03-0242ac130003')
    expect(mockBookRepo.upsertBook).not.toHaveBeenCalled()
    expect(res.status).toHaveBeenCalledWith(404)
    expect(res.json).toHaveBeenCalledWith({ response: 'The requested book \'bbfdc5bc-fe82-11eb-9a03-0242ac130003\' does not exist in the system.' })
  })
})
