Feature: Add/List

    Scenario: Receive book data
        Given book data is received
        When I ingest the book
        Then I should be able to list it
        